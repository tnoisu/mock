const Mock = require('../src/mock');

class JestMock extends Mock {
    verify() {
        expect(this.observed_calls).toEqual(this.expected_calls);
    }
}

module.exports = JestMock;
