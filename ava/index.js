const Mock = require('../src/mock');

class AvaMock extends Mock {
    verify(t) {
        t.deepEqual(this.observed_calls, this.expected_calls);
    }
}

module.exports = AvaMock
